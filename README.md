# hatari


## GFA Basic

![](https://gitlab.com/openbsd98324/hatari/-/raw/master/screenshot/190121-222124-screenshot.png)


## Hatari on Raspberry PI (Linux)

![](media/1638102768-screenshot.png)

![](media/1638102806-screenshot.png)

![](media/1638102965-screenshot.png)



## Hatari on NetBSD (Raspberry PI)


![](media/1643052479-screenshot.png)




## Hatari on FreeBSD (i386)

![](media/Screenshot_2022-02-19_13-05-47.png)


